import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatDrawer, MatDrawerContainer, MatSidenav } from '@angular/material/sidenav';
import { SidenavService } from '../services/sidenav.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  @Input() title: string;


  constructor(private sideNavService : SidenavService){
    this.sideNavService = sideNavService;
  }

  ngOnInit(): void {
  }

  toggleSideNav():void{
    this.sideNavService.toggle();
  }

}
