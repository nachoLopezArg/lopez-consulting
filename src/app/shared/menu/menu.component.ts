import {FlatTreeControl} from '@angular/cdk/tree';
import {Component} from '@angular/core';
import {MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material/tree';

/**
 * Food data with nested structure.
 * Each node has a name and an optional list of children.
 */
interface MenuNode {
  icon?: string;
  name: string;
  children?: MenuNode[];
}

const TREE_DATA: MenuNode[] = [
  {
    icon: 'fitness_center',
    name: 'Solid Concepts',
    children: [
      {name: 'Single Responsability'},
      {name: 'Open / Close'},
      {name: 'Liskov Substitution'},
      {name: 'Interface Segregation'},
      {name: 'Dependency Injection'}

    ]
  }, {
    icon: 'design_services',
    name: 'Design Patterns',
    children: [
      {name: 'Soon !'},

    ]
      }, 
    ]
;

/** Flat node with expandable and level information */
interface ExampleFlatNode {
  expandable: boolean;
  name: string;
  level: number;
}


@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent   {
   subitemColor : String = "primary";
  private _transformer = (node: MenuNode, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      name: node.name,
      icon: node.icon,
      level: level,
    };
  }

  treeControl = new FlatTreeControl<ExampleFlatNode>(
      node => node.level, node => node.expandable);

  treeFlattener = new MatTreeFlattener(
      this._transformer, node => node.level, node => node.expandable, node => node.children);

  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  constructor() {
    this.dataSource.data = TREE_DATA;
  }

  hasChild = (_: number, node: ExampleFlatNode) => node.expandable;


 

}
