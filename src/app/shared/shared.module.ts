import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavComponent } from './nav/nav.component';
import { FooterComponent } from './footer/footer.component';
import { MenuComponent } from './menu/menu.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import { BodyComponent } from './body/body.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatTreeModule} from '@angular/material/tree';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import { MdViewerComponent } from './md-viewer/md-viewer.component';
import { MarkdownModule } from 'ngx-markdown';
import { HttpClient, HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [NavComponent, FooterComponent, MenuComponent, BodyComponent, MdViewerComponent],
  imports: [
    CommonModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatTreeModule,
    MatButtonModule,
    MatCardModule,
    HttpClientModule,
    MarkdownModule.forRoot({ loader: HttpClient }),  ],
  exports:[
    NavComponent,
    BodyComponent,
    MenuComponent
  ],
})
export class SharedModule { }
